from __future__ import print_function
import json, sys, argparse

ap = argparse.ArgumentParser("wrap JSON in either a variable declaration (hardcode) or a callback (JSONP).")
ap.add_argument("--variable", default="weft", help="define a variable")
ap.add_argument("--callback", help="use a named callback (JSONP) -- overrides --variable")
args = ap.parse_args()

d = json.load(sys.stdin)
if args.callback:
    print ("{0}({1});".format(args.callback, json.dumps(d)))
else:
    print ("{0} = {1};".format(args.variable, json.dumps(d)))
