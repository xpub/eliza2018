%.json: %.txt
	python3 eliza_script_to_json.py < $< > $@
%.js: %.json
	python3 jsonp.py --callback "eliza" < $< > $@

doctor.html: doctor.json
	python3 json_to_print.py $< rules.template.html > $@
